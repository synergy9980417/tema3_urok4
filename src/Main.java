//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
//        System.out.printf("Hello and welcome!");
//
//        for (int i = 1; i <= 5; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            System.out.println("i = " + i);
//        }


//        Урок 4. Ключевое слово this. Экземпляры класса
//        Цель задания:
//        Совершенствование навыков работы с классами, знакомство с ключевым
//        словом this
//        Задание:
//        1.
//        Зачем нужно ключевое слово this?
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Чтобы обращаться к эземпляру класса внутри методов класса, из области видимости класса");



//                2.
//        Можно ли обратиться к полям класса, не используя ключевое слово this?
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("да");



//                Как?
//                        3.
//        Когда стоит использовать слово this?


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("желательно всегда. т.к. это исключает конфликт имён. и тогда когда из метода нужно вернуть экземпляр класса без  this не обойтись");

//                4.
//        Создайте класс, который находит наибольшее из трех чисел. Ко всем
//        переменным обращаться можно ТОЛЬКО используя this.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!");
        Test4 t1=new Test4(1,2,3);
        System.out.println("Maximum = "+t1.max);

//        5.
//        Создайте класс, который дел
//        ает из трех маленьких строк одну большую. Ко
//        всем переменным обращаться можно ТОЛЬКО используя this.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!");

        Test5 t2=new Test5("1111","22222","33333");
        System.out.println("SumStr = "+t2.sumStr);
//        6.
//        Создайте класс, который принимает аргументом, какую задачу он должен
//        решить: “найти минимум из 2 чисел”, “найти сумму элементов массива”,
//“вывести строку с
//        конца”. На выходе этот класс пишет в консоль Java

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!");

        Test6 t6_1=new Test6(1);

        Test6 t6_2=new Test6(2);

        Test6 t6_3=new Test6(3);
//        -
//                код,
//                который решает заданную задачу. Ко всем переменным обращаться можно
//        ТОЛЬКО используя this.
//                Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура программы
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно техническое задание от 80%, а также
//        продемонстрированы теоретические знания на удовлетворительном уровне
//        5 баллов
//                -
//                все технические задания выполнены корре
//        ктно, в полном объеме,
//        продемонстрированы  отличные теоретические знания при ответе на вопросы
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов

    }

    static class Test4 {
        int max;
        public Test4(int x, int y, int z){
            this.max=0;
            if (x>=y&&x>=z) this.max=x;
            else if (y>=x&&y>=z) this.max=y;
            else if (z>=x&&z>=y) this.max=z;

        }
    }

    static class Test5 {
        String sumStr="";
        String x="",y="",z="";
        public Test5(String x, String y, String z){
           this.x=x;
           this.y=y;
           this.z=z;
           this.sumStr=this.x+this.y+this.z;
        }
    }


    static class Test6 {
        //задание 1
        String task1="найти минимум из 2 чисел";
        //задание 2
        String task2="найти сумму элементов массива";
        //задание 3
        String task3="вывести строку с конца";

        //при создании экземляра класса нужно указывать какое задание он выполнит 1,2  или 3
        public Test6(int t){
            System.out.println("Создан следующий экземпляр класса:");
        if (t==1) System.out.println(task1);
            if (t==2) System.out.println(task2);

            if (t==3) System.out.println(task3);

        }
    }

}